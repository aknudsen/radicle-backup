use super::error;
use serde_json::Value;
use std::process::Command;

pub fn get_url() -> Result<String, error::Error> {
    let output: String = match Command::new("rad")
        .args(&["ipfs", "config", "show"])
        .output()
    {
        Ok(output) => Ok(String::from_utf8(output.stdout)?),
        Err(err) => Err(format!("Failed to get Radicle IPFS configuration: {}", err)),
    }?;
    let v: Value = serde_json::from_str(output.as_str())?;
    let api_maddr = String::from(v["Addresses"]["API"].as_str().unwrap());
    let re_maddr = regex::Regex::new(r"^/ip4/([^/]+)/tcp/(\d+)")?;
    let cap = match re_maddr.captures(api_maddr.as_str()) {
        Some(cap) => Ok(cap),
        None => Err(error::Error::Error(
            "Invalid API multi-address from rad ipfs".to_string(),
        )),
    }?;
    let url = format!("http://{}:{}", &cap[1], &cap[2]);
    return Ok(url);
}
