use std::collections::HashMap;

#[derive(Deserialize, Serialize, Debug)]
pub struct RadicleBlock {
    #[serde(default)]
    pub expressions: Vec<String>,
    #[serde(default)]
    pub previous: HashMap<String, String>,
}

impl RadicleBlock {
    pub fn new(expressions: Vec<String>, previous: String) -> RadicleBlock {
        let mut previous_map = HashMap::new();
        previous_map.insert("/".to_string(), previous);
        RadicleBlock {
            expressions: expressions,
            previous: previous_map,
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct RadicleStateMachine {
    pub id: String,
    pub tp: String,
}

impl RadicleStateMachine {
    pub fn new(id: String, tp: String) -> RadicleStateMachine {
        RadicleStateMachine { id: id, tp: tp }
    }
}
