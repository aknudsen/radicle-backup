use super::error;
use super::ipfs;
use super::radicle::RadicleBlock;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::Read;
use std::path::Path;
use std::process;
use std::process::Command;

fn unpack_entries<R: Read>(
    dst_path: &Path,
    proj_name: &String,
    entries: tar::Entries<R>,
) -> Result<(), error::Error> {
    let proj_path = dst_path.join(proj_name);
    if proj_path.exists() {
        return Err(error::Error::Error(format!(
            "Destination already exists: {}",
            proj_path.to_str().unwrap()
        )));
    }

    println!("* Unpacking archive into {}", dst_path.to_str().unwrap());
    for result in entries {
        let mut entry = result?;
        // println!(
        //     "* Unpacking entry {} into {}",
        //     entry.path()?.to_str().unwrap(),
        //     dst_path.to_str().unwrap()
        // );
        entry.unpack_in(dst_path)?;
    }

    Ok(())
}

fn get_project_id(tree: &sled::Db) -> Result<String, error::Error> {
    match tree.get("projectId")? {
        Some(project_id) => Ok(bincode::deserialize(&project_id)?),
        None => Err(error::Error::Error(String::from(
            "Can't get field projectId from database",
        ))),
    }
}

fn get_num_machines(tree: &sled::Db) -> Result<u64, error::Error> {
    match tree.get("numMachines")? {
        Some(num_machines) => Ok(bincode::deserialize(&num_machines)?),
        None => Err(error::Error::Error(String::from(
            "Can't get field numMachines from database",
        ))),
    }
}

fn get_num_blocks(tree: &std::sync::Arc<sled::Tree>) -> Result<i64, error::Error> {
    match tree.get("numBlocks")? {
        Some(num_blocks) => Ok(bincode::deserialize(&num_blocks)?),
        None => Err(error::Error::Error(String::from(
            "Can't get field numBlocks from database",
        ))),
    }
}

fn get_machine_name(tree: &std::sync::Arc<sled::Tree>) -> Result<String, error::Error> {
    match tree.get("name")? {
        Some(tp) => Ok(bincode::deserialize(&tp)?),
        None => Err(error::Error::Error(String::from(
            "Can't get field name from database",
        ))),
    }
}

fn get_machine_ipns_addr(tree: &std::sync::Arc<sled::Tree>) -> Result<String, error::Error> {
    match tree.get("ipnsAddress")? {
        Some(addr) => Ok(bincode::deserialize(&addr)?),
        None => Err(error::Error::Error(String::from(
            "Can't get field ipnsAddress from database",
        ))),
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct PutResponse {
    #[serde(default, rename = "Cid")]
    pub cid: HashMap<String, String>,
}

fn publish_block(
    exprs: Vec<String>,
    previous: String,
    ipfs_host: &String,
    client: &reqwest::Client,
) -> Result<String, error::Error> {
    let block = RadicleBlock::new(exprs, previous);
    let ser_block = serde_json::to_string(&block)?;
    let url = format!("{}/api/v0/dag/put?pin=true", ipfs_host);
    let form = reqwest::multipart::Form::new().part(
        "file",
        reqwest::multipart::Part::text(ser_block)
            .file_name("file")
            .mime_str("application/json")?,
    );
    let mut resp = client.post(url.as_str()).multipart(form).send()?;
    let put_resp: PutResponse = resp.json()?;
    match put_resp.cid.get("/") {
        Some(cid) => {
            println!("* Successfully added block, CID: {}", cid.to_string());
            Ok(cid.to_string())
        }
        None => Err(error::Error::Error(String::from(
            "Got invalid response from IPFS DAG put request",
        ))),
    }
}

fn restore_blocks_based_rsm(
    machine_name: String,
    machine_tree: std::sync::Arc<sled::Tree>,
    client: &reqwest::Client,
) -> Result<(), error::Error> {
    let num_blocks = get_num_blocks(&machine_tree)?;
    println!("* Blocks: {}", num_blocks);
    let ipfs_host = ipfs::get_url()?;
    // The initial value is the empty block ({"radicle":true})
    let mut prev_addr = String::from("zdpuAyyGtvC37aeZid2zh7LAGKCbFTn9MzdqoPpbNQm3BCwWT");
    for i in (0..num_blocks).rev() {
        let exprs_b = match machine_tree.get(format!("blocks/{}", i))? {
            Some(block) => Ok(block),
            None => Err(error::Error::Error(format!(
                "Couldn't get expression block #{} for {} Radicle state machine",
                i, machine_name
            ))),
        }?;
        let exprs: Vec<String> = bincode::deserialize(&exprs_b)?;
        println!(
            "* Publishing expression block #{} to IPFS, {} expression(s), previous: {}",
            i + 1,
            exprs.len(),
            prev_addr,
        );
        prev_addr = publish_block(exprs, prev_addr, &ipfs_host, &client)?;
    }

    let ipns_addr = get_machine_ipns_addr(&machine_tree)?;
    // Publish IPNS entry pointing to latest machine block
    let url = format!(
        "{}/api/v0/name/publish?arg={}&key={}",
        ipfs_host,
        format!("/ipfs/{}", prev_addr),
        ipns_addr
    );
    client.post(url.as_str()).send()?;
    println!(
        "* Published {} Radicle state machine IPNS record: {} => {}",
        machine_name, ipns_addr, prev_addr
    );

    Ok(())
}

/// Restore a Radicle state machine from the database.
fn restore_machine(
    machine_no: u64,
    tree: &sled::Db,
    client: &reqwest::Client,
) -> Result<(), error::Error> {
    let machine_tree = tree.open_tree(format!("machines/{}", machine_no))?;

    let machine_name = get_machine_name(&machine_tree)?;
    println!(
        "* Restoring machine #{}: {}...",
        machine_no + 1,
        machine_name
    );
    restore_blocks_based_rsm(machine_name, machine_tree, client)
}

fn restore_radicle_state(abs_path: &Path) -> Result<(), error::Error> {
    println!("* Restoring Radicle state...");

    let db_path = abs_path.join("radicle.db");
    println!("* Restoring from {}", db_path.to_str().unwrap());
    let tree = sled::Db::start_default(&db_path)?;
    let project_id = get_project_id(&tree)?;
    println!("* Project ID: {}", project_id);
    let num_machines = get_num_machines(&tree)?;
    println!("* Num machines: {}", num_machines);
    let client = reqwest::Client::builder().build()?;

    for i in 0..num_machines {
        restore_machine(i, &tree, &client)?;
    }

    fs::remove_dir_all(db_path.to_str().unwrap())?;

    println!("* Successfully restored Radicle state");

    Ok(())
}

fn open_archive(archive_path: String) -> Result<File, error::Error> {
    match File::open(&archive_path) {
        Ok(tarf) => Ok(tarf),
        Err(_) => {
            eprintln!("* Cannot open {}!", archive_path);
            process::exit(1)
        }
    }
}

fn convert_bare_repo(proj_path: &Path) -> Result<(), error::Error> {
    match Command::new("git")
        .current_dir(proj_path)
        .args(&["reset", "-q", "--hard"])
        .status()
    {
        Ok(_) => Ok(()),
        Err(err) => Err(error::Error::Error(format!(
            "Failed to convert repo to non-bare: {}",
            err
        ))),
    }
}

pub fn restore(archive_path: String, dst_path: String, dry_run: bool) -> Result<(), error::Error> {
    let mode_str = if dry_run { " (dry run)" } else { "" };
    println!("* Restoring {}{}", archive_path, mode_str);

    let tarf = open_archive(archive_path)?;
    let decoder = brotli::Decompressor::new(tarf, 4096);
    let mut ar = tar::Archive::new(decoder);
    let mut entries = ar.entries()?;
    let proj_name: String = match entries.next() {
        Some(first_entry_result) => {
            let first_entry = first_entry_result?;
            let first_path = first_entry.path()?;
            let proj_name = String::from(first_path.to_str().unwrap().trim_end_matches("/"));
            Ok(proj_name)
        }
        None => Err(error::Error::Error(String::from("Empty backup archive"))),
    }?;
    println!("* Project name is {}", proj_name);

    let abs_path = Path::new(&dst_path);
    let proj_path = abs_path.join(&proj_name);
    unpack_entries(&abs_path, &proj_name, entries)?;
    restore_radicle_state(&proj_path)?;
    convert_bare_repo(&proj_path)?;

    println!(
        "* Successfully restored project to {}",
        abs_path.to_str().unwrap()
    );
    Ok(())
}
