use std::{fmt, io};

#[derive(Debug)]
pub enum Error {
    Error(String),
    IO(std::io::Error),
    Ini(ini::ini::Error),
    Reqwest(reqwest::Error),
    Sled(sled::Error),
    Bincode(Box<bincode::ErrorKind>),
    SerdeJson(serde_json::Error),
    FromUtf8Error(std::string::FromUtf8Error),
    RegexError(regex::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Error(s) => f.write_str(s),
            Error::IO(e) => e.fmt(f),
            Error::Ini(e) => e.fmt(f),
            Error::Reqwest(e) => e.fmt(f),
            Error::Sled(e) => e.fmt(f),
            Error::Bincode(e) => e.fmt(f),
            Error::SerdeJson(e) => e.fmt(f),
            Error::FromUtf8Error(e) => e.fmt(f),
            Error::RegexError(e) => e.fmt(f),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::Error(_) => None,
            Error::IO(e) => Some(e),
            Error::Ini(e) => Some(e),
            Error::Reqwest(e) => Some(e),
            Error::Sled(e) => Some(e),
            Error::Bincode(e) => Some(e),
            Error::SerdeJson(e) => Some(e),
            Error::FromUtf8Error(e) => Some(e),
            Error::RegexError(e) => Some(e),
        }
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::IO(e)
    }
}

impl From<ini::ini::Error> for Error {
    fn from(e: ini::ini::Error) -> Self {
        Error::Ini(e)
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Error::Reqwest(e)
    }
}

impl From<sled::Error> for Error {
    fn from(e: sled::Error) -> Self {
        Error::Sled(e)
    }
}

impl From<Box<bincode::ErrorKind>> for Error {
    fn from(e: Box<bincode::ErrorKind>) -> Self {
        Error::Bincode(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::SerdeJson(e)
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(e: std::string::FromUtf8Error) -> Self {
        Error::FromUtf8Error(e)
    }
}

impl From<regex::Error> for Error {
    fn from(e: regex::Error) -> Self {
        Error::RegexError(e)
    }
}

impl From<&'static str> for Error {
    fn from(e: &'static str) -> Self {
        Error::Error(String::from(e))
    }
}

impl From<String> for Error {
    fn from(e: String) -> Self {
        Error::Error(e)
    }
}
