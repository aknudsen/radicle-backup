extern crate brotli;
extern crate clap;
extern crate ini;
extern crate reqwest;
extern crate tar;
extern crate tempfile;
#[macro_use]
extern crate serde;
extern crate bincode;
extern crate chrono;
extern crate regex;
extern crate serde_json;
extern crate sled;

use clap::{App, AppSettings, Arg, SubCommand};

mod backup;
mod error;
mod ipfs;
mod radicle;
mod restore;

fn main() -> Result<(), error::Error> {
    let version = env!("CARGO_PKG_VERSION");
    let matches = App::new("Radicle Backup")
        .version(version)
        .author("Arve Knudsen <arve.knudsen@gmail.com>")
        .about("Lets you back up your Radicle projects to DigitalOcean Spaces")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .arg(
            Arg::with_name("dry-run")
                .short("n")
                .long("dry-run")
                .global(true)
                .takes_value(false)
                .help("Sets whether to simulate actions [default: false]"),
        )
        .subcommand(
            SubCommand::with_name("backup").about("makes a backup").arg(
                Arg::with_name("project")
                    .required(true)
                    .help("Project to back up"),
            ),
        )
        .subcommand(
            SubCommand::with_name("restore")
                .about("restores a backup")
                .arg(
                    Arg::with_name("archive")
                        .required(true)
                        .help("Backup archive to restore"),
                )
                .arg(
                    Arg::with_name("destination")
                        .required(true)
                        .help("Destination directory"),
                ),
        )
        .get_matches();
    let dry_run = matches.occurrences_of("dry-run") > 0;
    match matches.subcommand_name() {
        Some("backup") => {
            let sub_matches = matches.subcommand_matches("backup").unwrap();
            backup::backup(
                String::from(sub_matches.value_of("project").unwrap()),
                dry_run,
            )
        }
        Some("restore") => {
            let sub_matches = matches.subcommand_matches("restore").unwrap();
            restore::restore(
                String::from(sub_matches.value_of("archive").unwrap()),
                String::from(sub_matches.value_of("destination").unwrap()),
                dry_run,
            )
        }
        _ => panic!("Unrecognized subcommand"),
    }
}
