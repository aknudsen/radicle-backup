use super::error;
use super::ipfs;
use super::radicle::{RadicleBlock, RadicleStateMachine};
use chrono::Utc;
use regex::Regex;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::Write;
use std::path::Path;
use std::process::{Command, Output};
use tempfile::NamedTempFile;

static RE_IPNS: &str = "^(?:ipfs://)?ipns/([a-zA-Z0-9]+)$";

/// Store a Radicle state machine based on a linked list of blocks in database.
fn store_blocks_based_rsm(
    rsm: &RadicleStateMachine,
    machine_no: u64,
    machine_name: &String,
    tree: &sled::Db,
) -> Result<(), error::Error> {
    let mut num_blocks: u64 = 0;
    let re_ipns = Regex::new(RE_IPNS)?;
    let res: Result<(String, String), error::Error> = match re_ipns.captures(rsm.id.as_str()) {
        Some(caps) => {
            let ipns_addr = caps.get(1).unwrap().as_str().to_string();
            let ipfs_addr = lookup_ipns_addr(&ipns_addr)?;
            Ok((ipns_addr, ipfs_addr))
        }
        None => Ok(("".to_string(), rsm.id.to_owned())),
    };
    let (ipns_addr, ipfs_addr) = res?;
    let machine_tree = tree.open_tree(format!("machines/{}", machine_no))?;

    let mut cur_addr: String = ipfs_addr.to_owned();
    let ipfs_base_url = ipfs::get_url()?;
    println!("* Using IPFS URL {}", ipfs_base_url);
    while !cur_addr.is_empty() {
        let block_text =
            reqwest::get(format!("{}/api/v0/dag/get?arg={}", ipfs_base_url, cur_addr).as_str())?
                .text()?;
        // TODO: Deal with unrecognized fields
        let block: RadicleBlock = serde_json::from_str(block_text.as_str())?;
        if !block.expressions.is_empty() {
            let encoded: Vec<u8> = bincode::serialize(&block.expressions)?;
            machine_tree.set(format!("blocks/{}", num_blocks.to_string()), encoded)?;
            num_blocks += 1;
        }
        if !block.previous.is_empty() {
            match block.previous.get("/") {
                Some(prev_addr) => cur_addr = prev_addr.to_owned(),
                None => cur_addr = String::from(""),
            }
        } else {
            cur_addr = String::from("");
        }
    }
    println!(
        "* Finished getting {} RSM blocks, found {} in total",
        machine_name, num_blocks
    );

    machine_tree.set("name", bincode::serialize(machine_name)?)?;
    machine_tree.set("numBlocks", bincode::serialize(&num_blocks)?)?;
    if ipns_addr.is_empty() {
        panic!(format!("No IPNS address detected for {} RSM", machine_name));
    }
    machine_tree.set("ipnsAddress", bincode::serialize(&ipns_addr.as_str())?)?;
    machine_tree.flush()?;

    Ok(())
}

fn add_repo(proj_path: &String, dst: &String, dry_run: bool) -> Result<(), error::Error> {
    let git_path = Path::new(dst.as_str()).join(".git");
    let git_dst = match git_path.to_str() {
        Some(path) => Ok(path),
        None => Err(error::Error::Error(String::from("Couldn't get git dir"))),
    }?;
    if !dry_run {
        match Command::new("git")
            .args(&["clone", "-q", "--bare", proj_path.as_str(), git_dst])
            .status()
        {
            Ok(_) => {
                println!("* Successfully added Git state to backup");
                Ok(())
            }
            Err(err) => Err(error::Error::Error(format!(
                "Failed to clone repo: {}",
                err
            ))),
        }?;
        // Keep .git/config since when cloning in bare mode, it changes
        fs::copy(
            Path::new(proj_path).join(".git/config"),
            git_path.join("config"),
        )?;
        Ok(())
    } else {
        println!("Cloning {} to {}", proj_path, git_dst);
        Ok(())
    }
}

/// Store a Radicle state machine, pulled from IPFS, in database.
fn store_rsm(
    rsm: &RadicleStateMachine,
    machine_no: u64,
    tree: &sled::Db,
) -> Result<(), error::Error> {
    let machine_name = String::from(match rsm.tp.as_str() {
        "project" => Ok("project"),
        "patch" => Ok("patches"),
        "issue" => Ok("issues"),
        _ => Err(error::Error::Error(format!(
            "Unrecognized RSM type {}",
            rsm.tp
        ))),
    }?);
    println!(
        "* Backing up {} RSM, IPNS address: {}...",
        machine_name, rsm.id
    );

    store_blocks_based_rsm(rsm, machine_no, &machine_name, tree)
}

// Look up IPNS address.
fn lookup_ipns_addr(ipns_addr: &String) -> Result<String, error::Error> {
    let resp: HashMap<String, String> = reqwest::get(
        format!(
            "http://127.0.0.1:9301/api/v0/name/resolve?arg=/ipns/{}",
            ipns_addr
        )
        .as_str(),
    )?
    .json()?;
    let re_ipfs = Regex::new("^(?:/ipfs/)?(.+)$")?;
    match resp.get("Path") {
        Some(path) => {
            let caps = re_ipfs.captures(path).unwrap();
            let addr = caps.get(1).unwrap().as_str().to_string();
            println!(
                "* Resolved IPNS address {} to IPFS address {}",
                ipns_addr, addr
            );
            Ok(addr)
        }
        None => Err(error::Error::Error(format!(
            "Couldn't resolve IPNS address {}",
            ipns_addr
        ))),
    }
}

fn make_get_rsms() -> Result<NamedTempFile, error::Error> {
    let mut f = NamedTempFile::new()?;
    write!(
        f,
        r#"(load! (find-module-file! "prelude.rad"))
(load! (find-module-file! "monadic/project.rad"))

(def format-machine (fn [machine] (list (lookup :id machine) (lookup :type machine))))

(def machines-to-backup
  "Get all the Radicle state machines for a project."
  (fn [proj-cid] (map format-machine (list-rsms! proj-cid)))
)

(def args (get-args!))

(match args
  ['proj] (print! (machines-to-backup proj))
  'r      (put-str! "expecting single argument: project CID")
)
"#
    )?;
    Ok(f)
}

fn get_rsms(project_id: &String) -> Result<Vec<RadicleStateMachine>, error::Error> {
    let re_ipns = Regex::new(RE_IPNS)?;
    let script_file = make_get_rsms()?;
    let output: Output = Command::new("radicle")
        .args(&[script_file.path().to_str().unwrap(), project_id])
        .output()
        .map_err(|err| {
            format!(
                "Failed to list Radicle state machines for project ID {}: {}",
                project_id, err
            )
        })?;
    if !output.status.success() {
        return Err(error::Error::Error(format!(
            "Failed to list Radicle state machines for project ID {}: {}",
            project_id,
            String::from_utf8_lossy(output.stdout.as_slice())
        )));
    }

    // The output from get-rsms should be on the form [("<address>" :rad-<type>)\n...]
    let mut data = String::from_utf8(output.stdout)?.trim().to_string();
    // Remove starting [
    data.remove(0);
    // Remove ending ]
    data.remove(data.len() - 1);
    let re_rsm = Regex::new("^\\(\"([^\"]+)\" :rad-(.+)\\)$")?;
    let rsms_res: Result<Vec<RadicleStateMachine>, error::Error> = data
        .split("\n")
        .map(|l| {
            let caps = re_rsm
                .captures(l)
                .ok_or(format!("Invalid get-rsms output: {}", l))?;
            let rsm_id = caps.get(1).unwrap().as_str().to_string();
            let rsm_type = caps.get(2).unwrap().as_str().to_string();
            let ipns_addr = if re_ipns.is_match(rsm_id.as_str()) {
                rsm_id
            } else {
                format!("ipns/{}", rsm_id)
            };
            Ok(RadicleStateMachine::new(ipns_addr, rsm_type))
        })
        .filter(|rsm_res| match rsm_res {
            Ok(rsm) => rsm.tp != "repo",
            Err(_) => true,
        })
        .collect();
    let mut rsms = rsms_res?;
    rsms.insert(
        0,
        RadicleStateMachine::new(format!("ipns/{}", project_id), "project".to_string()),
    );
    Ok(rsms)
}

fn add_radicle_state(
    proj_path: &String,
    backup_path: &String,
    _dry_run: bool,
) -> Result<(), error::Error> {
    println!("* Getting Radicle state...");
    let git_config = ini::Ini::load_from_file(Path::new(proj_path).join(".git/config"))?;
    let project_id = match git_config.section(Some("radicle")) {
        Some(section) => match section.get("project-id") {
            Some(project_id) => Ok(project_id.to_string()),
            None => Err(error::Error::Error(
                "Couldn't find radicle.project-id in .git/config".to_string(),
            )),
        },
        None => Err(error::Error::Error(
            "Couldn't find radicle section in .git/config".to_string(),
        )),
    }?;

    println!(
        "* Adding Radicle state for project {} to backup...",
        project_id
    );
    let path = Path::new(backup_path).join("radicle.db");
    let tree = sled::Db::start_default(path)?;

    let rsms = get_rsms(&project_id)?;
    for (i, rsm) in rsms.iter().enumerate() {
        store_rsm(&rsm, i as u64, &tree)?;
    }
    tree.set("projectId", bincode::serialize(&project_id)?)?;
    tree.set("numMachines", bincode::serialize(&rsms.len())?)?;
    tree.flush()?;

    println!("* Successfully added Radicle state to backup");
    Ok(())
}

fn make_archive(
    proj_name: &String,
    backup_path: &String,
    dry_run: bool,
) -> Result<(), error::Error> {
    println!("* Making backup archive...");
    let date_str = Utc::now().format("%Y-%m-%d");
    let fpath = format!("{}-backup-{}.tar.br", proj_name, date_str);
    if !dry_run {
        let tarf = File::create(&fpath)?;
        let encoder = brotli::CompressorWriter::new(tarf, 4096, 11, 20);
        let mut ar = tar::Builder::new(encoder);
        ar.append_dir_all(proj_name, &backup_path)?;
        ar.finish()?;
        println!("* Successfully wrote backup archive to {}", fpath);
    } else {
        println!("* Writing backup archive to {}", fpath);
    }
    Ok(())
}

pub fn backup(proj_path: String, dry_run: bool) -> Result<(), error::Error> {
    let mode_str = if dry_run { " (dry run)" } else { "" };
    let components: Vec<String> = Path::new(&proj_path)
        .components()
        .map(|x| String::from(x.as_os_str().to_str().unwrap()))
        .collect();
    let proj_name = components.last().unwrap();
    println!("* Backing up project {}{}!", proj_name, mode_str);
    let dir = tempfile::tempdir()?;
    let dst_path = match dir.path().to_str() {
        Some(path) => Ok(String::from(path)),
        None => Err(error::Error::Error(String::from("Couldn't get temp dir"))),
    }?;
    add_repo(&proj_path, &dst_path, dry_run)?;
    add_radicle_state(&proj_path, &dst_path, dry_run)?;

    make_archive(&proj_name, &dst_path, dry_run)?;

    // TODO: Upload archive
    println!("* Successfully backed up");
    Ok(())
}
